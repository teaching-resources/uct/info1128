
import matplotlib.pyplot as plt
import numpy as np

from PIL import Image, ImageOps

""" rgb_image = Image.open("./amazon.jpg")

print(rgb_image)

grayscale_image = ImageOps.grayscale(rgb_image)

print(grayscale_image)

grayscale_hst = grayscale_image.histogram()

print(grayscale_hst)

Grey = grayscale_hst[0:256]  # indicates Grey

print(Grey) """

import cv2

img = cv2.imread('./amazon.jpg')

gray = cv2.imread('./amazon.jpg', cv2.IMREAD_GRAYSCALE)

# print(gray)

ret, temp_show = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY_INV)
ret, thresh = cv2.threshold(gray, 200, 1, cv2.THRESH_BINARY_INV)

print(thresh)

#cv2.imshow('thresh', temp_show)
# cv2.waitKey()

thresh1 = np.asarray(thresh)

print(thresh1)

temp = thresh1.astype(int)

print(temp)
temp = list(temp)

print(temp)
np.savetxt('data.csv', temp, delimiter=', ')

M_00 = np.sum(thresh1)

print(M_00)

xx, yy = np.where(thresh1 == 1)

print(xx)
print(yy)

M_10 = np.sum(xx)
M_01 = np.sum(yy)

print(M_10)
print(M_01)

cx = int(M_10/M_00)
cy = int(M_01/M_00)

print('CX: ' + str(cx) + ' CY: ' + str(cy))

cv2.circle(img, (cx, cy), 5, (0, 0, 255), -1)
cv2.putText(img, "Centroid", (cx - 25, cy - 25),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)

# find outer contour
cntrs = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cntrs = cntrs[0] if len(cntrs) == 2 else cntrs[1]

# get rotated rectangle from outer contour
rotrect = cv2.minAreaRect(cntrs[0])
box = cv2.boxPoints(rotrect)
box = np.int0(box)

# draw rotated rectangle on copy of img as result
cv2.drawContours(img, [box], 0, (0, 0, 255), 2)

cv2.imwrite('centroid.jpg', img)
