import webbrowser
from PIL import Image

# file = "./amazon.jpg"
# img = Image.open(file)

# absolute path
""" folder_path = "/home/nicolas/Projects/Dept INF/INFO1128/PIL_pillow_examples/"
for i in folder_path:
    try:
        img = Image.open(i)
        img.show()
        img.save("new_" + i)
    except Exception:
        pass """

# Apple = Image.open("apple.jpg")
# Apple.show()
# instead of using .open and .show(),
# save the image to show it with the webbrowser module

filename = "amazon.jpg"
webbrowser.open(filename)
