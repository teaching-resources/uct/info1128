import cv2

img = cv2.imread('rainbow.jpg')

# Set blue and green channels to 0
blue = img[:, :, 0] = 0  # blue channel
green = img[:, :, 1] = 0  # green channel
red = img[:, :, 2] = 0  # red channel

cv2.imshow('red_img', img)
cv2.waitKey()
