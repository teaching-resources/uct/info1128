import cv2

import numpy as np
from matplotlib import pyplot as plt

# sample_img = cv2.imread('./circle-768x768.jpg', cv2.IMREAD_GRAYSCALE)
original = cv2.imread('./yy.png')
gray = cv2.imread('./yy.png', cv2.IMREAD_GRAYSCALE)

# load image as HSV and select saturation
""" sample_img = cv2.imread("./circle-768x768.jpg")
hh, ww, cc = sample_img.shape """

# convert to gray
# gray = cv2.cvtColor(sample_img, cv2.COLOR_BGR2GRAY)

# threshold the grayscale image
ret, thresh1 = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY)
ret, thresh2 = cv2.threshold(gray, 200, 255, cv2.THRESH_BINARY_INV)
ret, thresh3 = cv2.threshold(gray, 200, 255, cv2.THRESH_TRUNC)
ret, thresh4 = cv2.threshold(gray, 200, 255, cv2.THRESH_TOZERO)
ret, thresh5 = cv2.threshold(gray, 200, 255, cv2.THRESH_TOZERO_INV)

titles = ['Original Image', 'BINARY',
          'BINARY_INV', 'TRUNC', 'TOZERO', 'TOZERO_INV']
images = [gray, thresh1, thresh2, thresh3, thresh4, thresh5]

# Treshold comparison
plt.figure(0)
for i in range(6):
    plt.subplot(2, 3, i+1), plt.imshow(images[i], 'gray', vmin=0, vmax=255)
    plt.title(titles[i])
    plt.xticks([]), plt.yticks([])

plt.figure(0).savefig('treshold-comparison.jpg')

# Moments
M = cv2.moments(thresh1)
print(M)

# Hu Moments
hu = cv2.HuMoments(M)
print(hu)

# Centroid
cx = int(M['m10']/M['m00'])
cy = int(M['m01']/M['m00'])
print('CX: ' + str(cx) + ' CY: ' + str(cy))

# Draw centroid
thresh_copy = original.copy()
cv2.circle(thresh_copy, (cx, cy), 5, (0, 0, 255), -1)
cv2.putText(thresh_copy, "Centroid", (cx - 25, cy - 25),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2)
cv2.imwrite('centroid.jpg', thresh_copy)

# Eccentricity
ecc = np.sqrt(1 - hu[0][0]**2)
print('Eccentricity: ' + str(ecc))

# Orientation
orientation = np.arctan2(hu[1][0], hu[0][0])
print('Orientation: ' + str(orientation))

# find outer contour
cntrs = cv2.findContours(thresh2, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
cntrs = cntrs[0] if len(cntrs) == 2 else cntrs[1]

# get rotated rectangle from outer contour
rotrect = cv2.minAreaRect(cntrs[0])
box = cv2.boxPoints(rotrect)
box = np.int0(box)

# draw rotated rectangle on copy of img as result
result = original.copy()
cv2.drawContours(result, [box], 0, (0, 0, 255), 2)

# get angle from rotated rectangle
angle = rotrect[-1]

# from https://www.pyimagesearch.com/2017/02/20/text-skew-correction-opencv-python/
# the `cv2.minAreaRect` function returns values in the
# range [-90, 0); as the rectangle rotates clockwise the
# returned angle trends to 0 -- in this special case we
# need to add 90 degrees to the angle
if angle < -45:
    angle = -(90 + angle)

# otherwise, just take the inverse of the angle to make
# it positive
else:
    angle = -angle

print(angle, "deg")

# write result to disk
cv2.imwrite("contour.jpg", result)
