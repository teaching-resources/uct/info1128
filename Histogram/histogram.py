from PIL import Image, ImageOps
import matplotlib.pyplot as plt


def getRed(R): return '#%02x%02x%02x' % (R, 0, 0)
def getGreen(G): return '#%02x%02x%02x' % (0, G, 0)
def getBlue(B): return '#%02x%02x%02x' % (0, 0, B)
def getGrey(G): return '#%02x%02x%02x' % (G, G, G)


rgb_image = Image.open("./rainbow.jpg")
rgb_hst = rgb_image.histogram()

grayscale_image = ImageOps.grayscale(rgb_image)
grayscale_hst = grayscale_image.histogram()

Red = rgb_hst[0:256]      # indicates Red
Green = rgb_hst[256:512]  # indicated Green
Blue = rgb_hst[512:768]   # indicates Blue
Grey = grayscale_hst[0:256]  # indicates Grey

plt.figure(0)
# plots a figure to display RED Histogram
for i in range(0, 256):
    plt.bar(i, Red[i], color=getRed(i), alpha=0.3)

plt.figure(1)
# plots a figure to display GREEN Histogram
for i in range(0, 256):
    plt.bar(i, Green[i], color=getGreen(i), alpha=0.3)

plt.figure(2)
# plots a figure to display BLUE Histogram
for i in range(0, 256):
    plt.bar(i, Blue[i], color=getBlue(i), alpha=0.3)

plt.figure(3)
# plots a figure to display GRAYSCALE Histogram
for i in range(0, 256):
    plt.bar(i, Grey[i], color=getGrey(i), alpha=0.3)


plt.figure(0).savefig('histogram-red.png')
plt.figure(1).savefig('histogram-green.png')
plt.figure(2).savefig('histogram-blue.png')
plt.figure(3).savefig('histogram-grey.png')

grayscale_image.save('rainbow-grey.png')
# plt.show()
