# https://www.johndcook.com/blog/2017/11/06/chebyshev-interpolation/

import matplotlib.pyplot as plt
from numpy import array, cos, pi
from scipy import interpolate, linspace


def cauchy(x):
    return (1 + x**2)**-1


n = 16
x = linspace(-5, 5, n)  # points to interpolate at

y = cauchy(x)
f = interpolate.BarycentricInterpolator(x, y)

xnew = linspace(-5, 5, 200)  # points to plot at
ynew = f(xnew)
plt.plot(x, y, 'o', xnew, ynew, '-')
plt.show()

# Interpolate using Chebyshev polynomials
x = [cos(pi*(2*k-1)/(2*n)) for k in range(1, n+1)]
x = 5*array(x)

y = cauchy(x)
f = interpolate.BarycentricInterpolator(x, y)

xnew = linspace(-5, 5, 200)  # points to plot at
ynew = f(xnew)
plt.plot(x, y, 'o', xnew, ynew, '-')
plt.show()
