import numpy as np
import scipy as sp
import matplotlib.pyplot as plt
from scipy import signal

# https://machinelearning1.wordpress.com/2014/07/13/signal-processing-apply-median-filters-python/

# https://en.wikipedia.org/wiki/Median_filter
# https://en.wikipedia.org/wiki/Wiener_filter

t = np.linspace(0, 10, 200)  # create a time signal
x1 = np.sin(t)  # create a simple sine wave
x2 = x1 + np.random.rand(200)  # add noise to the signal
y1 = sp.signal.medfilt(x2, 21)  # apply median filter to the signal
y2 = sp.signal.wiener(x2, 21)  # apply wiener filter to the signal
# plot the results
plt.subplot(4, 1, 1)
plt.plot(t, x1, 'yo-')
plt.title('Original wave')
plt.xlabel('Time')
plt.subplot(4, 1, 2)
plt.plot(t, x2, 'yo-')
plt.title('Noisy wave')
plt.xlabel('Time')
plt.subplot(4, 1, 3)
plt.plot(range(200), y1, 'yo-')
plt.title('Median filtered wave')
plt.xlabel('Time')
plt.subplot(4, 1, 4)
plt.plot(range(200), y2, 'yo-')
plt.title('Wiener filtered wave')
plt.xlabel('Time')
plt.tight_layout()
plt.show()
